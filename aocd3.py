input_name = "input.txt"

def source_data(input_name):
    result = []
    with open(input_name, "r") as f:
        for line in f:
            result.append(line.strip())
    return result

def total_count(bags):
    result = 0
    for bag in bags:
        result += encode_char((one_count(bag)))
    return result

def one_count(bag):
    middle = int(len(bag)/2)
    for e in bag[:middle]:
        if e in bag[middle:]:
            return e

def encode_char(charc):
    if charc >= "a":
        return ord(charc) - ord("a") + 1
    else:
        return ord(charc) - ord("A") + 27


bags = source_data(input_name)
x = total_count(bags)
print(x)